
#include "sync_common.h"
#include <arpa/inet.h>

int raw = 0;

void get_msg_and_timestamps(int sock, struct sockaddr_in* address,
			    void* buffer, int len,
			    uint64_t* sw_ts, uint64_t* hw_ts)
{
  struct msghdr msg;
  struct iovec iov;
  char control[1024];

  /* recvmsg header structure */
  iov.iov_base = buffer;
  iov.iov_len = len;
  msg.msg_iov = &iov;
  msg.msg_iovlen = 1;
  msg.msg_name = &address;
  msg.msg_namelen = sizeof(struct sockaddr_in);
  msg.msg_control = control;
  msg.msg_controllen = 1024;

  *sw_ts = 0;
  *hw_ts = 0;
  
  if(recvmsg(sock, &msg, 0) < 0) {
    printf("Failed to get msg for rx packet: %m\n");
    return;
  }
  parse_msg_timestamp(&msg, sw_ts, hw_ts);
};

int main(int argc, char** argv)
{
  int sync_socket;
  int sfup_socket;
  
  struct sockaddr_in sync_address;
  struct sockaddr_in sfup_address;

  msg_t sync_msg;
  msg_t sfup_msg;

  struct ip_mreq group;
  
  const char* dev = argv[1];

  if (argc < 2) {
    printf ("HW timestamps not used, provide name of device to enable hw timestamps\n");
  }
  
  sync_socket = create_socket();

  if (dev != NULL) {
    enable_hw_ts(sync_socket, dev);
  }
  
  enable_socket_timestamping(sync_socket);
  
  sfup_socket = create_socket();

  create_address(&sync_address, 0, sync_port);
  create_address(&sfup_address, 0, sfup_port);

  if (bind(sync_socket, (struct sockaddr*)&sync_address, sizeof(sync_address))) {
    printf("Unable to bind sync socket: %m\n");
  }

  if (bind(sfup_socket, (struct sockaddr*)&sfup_address, sizeof(sfup_address))) {
    printf("Unable to bind sfup socket: %m\n");
  }

  group.imr_multiaddr.s_addr = inet_addr(mcast_addr);
  group.imr_interface.s_addr = INADDR_ANY;
  
  if (setsockopt(sync_socket, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                 (char *)&group, sizeof(group)) < 0) {
    printf("Unable to add mcast membership on sync socket: %m\n");
  }
  
  if (setsockopt(sfup_socket, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                 (char *)&group, sizeof(group)) < 0) {
    printf("Unable to add mcast membership on sfup socket: %m\n");
  }
  msg_t last_offsets = {0};
  
  while (1) {
    uint64_t sw_timestamp;
    uint64_t hw_timestamp;
    
    socklen_t len = sizeof(sync_address);
    
    get_msg_and_timestamps(sync_socket, &sync_address,
			   &sync_msg, sizeof(sync_msg),
			   &sw_timestamp,
			   &hw_timestamp);

    uint64_t sw_clock = now();
    
    recvfrom(sfup_socket, &sfup_msg, sizeof(sfup_msg), 0,
	     (struct sockaddr*) &sfup_address, &len);
    
    msg_t offsets;
    offsets.sw_clock = sw_clock - sync_msg.sw_clock;
    offsets.sw_timestamp = sw_timestamp - sfup_msg.sw_timestamp;
    offsets.hw_timestamp = hw_timestamp - sfup_msg.hw_timestamp;

    if (sfup_msg.sw_timestamp) {
      if (raw){
	printf("%-20llu %-20llu %-20llu %-20llu %-20llu %-20llu \n", sync_msg.sw_clock,
	       sfup_msg.sw_clock, sfup_msg.sw_timestamp, sfup_msg.hw_timestamp, sw_timestamp, hw_timestamp);
      }
      else {
	printf("sw-clock: %12d sw-ts: %12d hw-ts: %12d tx-sw-hw-diff: %12d rx-sw-hw-diff: %12d\n",
	       offsets.sw_clock     - last_offsets.sw_clock,
	       offsets.sw_timestamp - last_offsets.sw_timestamp,
	       offsets.hw_timestamp - last_offsets.hw_timestamp,
	       sfup_msg.sw_timestamp - sfup_msg.hw_timestamp,
	       sw_timestamp - hw_timestamp);
      }
      last_offsets = offsets;
    }
  }
  
  close(sync_socket);
  close(sfup_socket);
  
  return 0;
}
