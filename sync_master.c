
#include "sync_common.h"

/* Receive a packet, and print out the timestamps from it */
void get_timestamps(int sock, uint64_t* sw_ts, uint64_t* hw_ts)
{
  struct msghdr msg;
  struct iovec iov;
  struct sockaddr_in host_address;
  char buffer[2048];
  char control[1024];

  /* recvmsg header structure */
  memset(&host_address, 0, sizeof(host_address));
  iov.iov_base = buffer;
  iov.iov_len = 2048;
  msg.msg_iov = &iov;
  msg.msg_iovlen = 1;
  msg.msg_name = &host_address;
  msg.msg_namelen = sizeof(struct sockaddr_in);
  msg.msg_control = control;
  msg.msg_controllen = 1024;

  *sw_ts = 0;
  *hw_ts = 0;
  
  while(recvmsg(sock, &msg, MSG_ERRQUEUE) != -1) {
    parse_msg_timestamp(&msg, sw_ts, hw_ts);
  }
};

int main(int argc, char** argv)
{
  int sync_socket;
  int sfup_socket;
  
  unsigned int packet_counter;
  
  struct sockaddr_in sync_address;
  struct sockaddr_in sfup_address;

  msg_t sync_msg = {0x11111111, 0, 0, 0};
  msg_t sfup_msg = {0x22222222, 0, 0, 0};

  uint64_t next;
  
  const char* dev = argv[1];

  if (argc < 2) {
    printf ("HW timestamps not used, provide name of device to enable hw timestamps\n");
  }
  
  sync_socket = create_socket();

  if (dev != NULL) {
    enable_hw_ts(sync_socket, dev);
  }
  
  enable_socket_timestamping(sync_socket);
  
  sfup_socket = create_socket();

  create_address(&sync_address, mcast_addr, sync_port);
  create_address(&sfup_address, mcast_addr, sfup_port);

  next = now() + 2 * sync_period_ns;
  
  /* Run forever */
  for (packet_counter = 0; packet_counter < 1000; packet_counter++) {
 
    sync_msg.id = packet_counter;
    sfup_msg.id = packet_counter;
    sync_msg.sw_clock = next;
    wait_until(next);
    
    sendto(sync_socket, &sync_msg, sizeof(sync_msg), 0,
	   (struct sockaddr*) &sync_address, sizeof(sync_address));

    sfup_msg.sw_clock = now();
    wait_until(next + 10000000); // Wait for timestamps

    get_timestamps(sync_socket,
		   &sfup_msg.sw_timestamp,
		   &sfup_msg.hw_timestamp);

    sendto(sfup_socket, &sfup_msg, sizeof(sfup_msg), 0,
	   (struct sockaddr*) &sfup_address, sizeof(sfup_address));
    
    printf("%20llu %20llu %20llu %20llu \n", sync_msg.sw_clock, sfup_msg.sw_clock, sfup_msg.sw_timestamp, sfup_msg.hw_timestamp);
    next += sync_period_ns;
  }

  close(sync_socket);
  close(sfup_socket);
  
  return 0;
}
