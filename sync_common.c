
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <errno.h>
#include <time.h>
#include <getopt.h>

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <arpa/inet.h>

#include <linux/net_tstamp.h>
#include <linux/sockios.h>

#include <unistd.h>
#include <fcntl.h>

#include "sync_common.h"

#define CLOCKFD 3
#define FD_TO_CLOCKID(fd)	((clockid_t) ((((unsigned int) ~fd) << 3) | CLOCKFD))

const char* mcast_addr = "224.0.0.1";

const int sync_port = 2000;
const int sfup_port = 2001;

const int sync_period_ns = 100 * 1000000; // 100 ms => 10 Hz

uint64_t nsec(struct timespec* ts)
{
  return 1000000000 * ts->tv_sec + ts->tv_nsec;
}

uint64_t clock_now(clockid_t clock)
{
  struct timespec ts;
  clock_gettime(clock, &ts);
  return nsec(&ts);
}

uint64_t now()
{
  return clock_now(CLOCK_MONOTONIC);
}

void nsec_to_ts(struct timespec* ts, uint64_t ns)
{
  ts->tv_sec  = ns / 1000000000;
  ts->tv_nsec = ns % 1000000000;
}

void wait_until(uint64_t ns)
{
  struct timespec ts;
  ts.tv_sec  = ns / 1000000000;
  ts.tv_nsec = ns % 1000000000;
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts, 0);
}

void enable_hw_ts(int sock, const char* dev)
{
  struct ifreq ifr;
  struct hwtstamp_config hwc;

  bzero(&ifr, sizeof(ifr));
  snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", dev);

  /* Standard kernel ioctl options */
  hwc.flags = 0;
  hwc.tx_type = HWTSTAMP_TX_ON;
  hwc.rx_filter = HWTSTAMP_FILTER_ALL;

  ifr.ifr_data = (char*)&hwc;

  
  if (ioctl(sock, SIOCSHWTSTAMP, &ifr) < 0 ) {
    printf("Unable to enable HW timestamps on %s: %m\n", dev);
  }
  return;
}

void enable_socket_timestamping(int sock)
{
  int enable = (SOF_TIMESTAMPING_RX_SOFTWARE |
		SOF_TIMESTAMPING_RX_HARDWARE |
		SOF_TIMESTAMPING_TX_SOFTWARE |
		SOF_TIMESTAMPING_TX_HARDWARE |
		SOF_TIMESTAMPING_SOFTWARE |
		SOF_TIMESTAMPING_OPT_CMSG |
		SOF_TIMESTAMPING_OPT_TX_SWHW |
		SOF_TIMESTAMPING_RAW_HARDWARE);
  
  if (setsockopt(sock, SOL_SOCKET, SO_TIMESTAMPING, &enable, sizeof(int)) < 0) {
    printf("Unable to enable timestamps on socket: %m\n");
  }
}

void create_address(struct sockaddr_in* address, const char* ip, const int port)
{
  
  bzero(address, sizeof(*address));

  address->sin_family = AF_INET;
  address->sin_port = htons(port);
  if (ip == 0) {
    address->sin_addr.s_addr = INADDR_ANY;
  }
  else {
    address->sin_addr.s_addr = inet_addr(ip);
  }
}

int create_socket(void)
{
  int s;
  
  s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (s < 0) {
    printf ("Unable to create socket: %m\n");
  }
  return s;
}

static clockid_t ptp_clock = -1;

clockid_t phc_open(const char *phc)
{
  clockid_t clkid;
  struct timespec ts;
  int fd;
  
  fd = open(phc, O_RDWR);
  if (fd < 0)
    return -1;
  
  clkid = FD_TO_CLOCKID(fd);
  /* check if clkid is valid */
  if (clock_gettime(clkid, &ts)) {
    close(fd);
    return -1;
  }
  return clkid;
}

void parse_msg_timestamp(struct msghdr* msg,
			 uint64_t* sw_ts,
			 uint64_t* hw_ts)
{
  struct timespec* ts = NULL;
  struct cmsghdr* cmsg;
  
  if (ptp_clock == -1) {
    ptp_clock = phc_open("/dev/ptp0");
  }

  int64_t ptp_offset = clock_now(ptp_clock) - now();
  int64_t realtime_offset = clock_now(CLOCK_REALTIME) - now();
  
  for( cmsg = CMSG_FIRSTHDR(msg); cmsg; cmsg = CMSG_NXTHDR(msg,cmsg) ) {
    if( cmsg->cmsg_level != SOL_SOCKET )
      continue;

    switch( cmsg->cmsg_type ) {
    case SO_TIMESTAMPING:
      ts = (struct timespec*) CMSG_DATA(cmsg);
      if (ts[0].tv_sec) {
	*sw_ts = nsec(&ts[0]) - realtime_offset;
      }
      if (ts[2].tv_sec) {
	*hw_ts = nsec(&ts[2]) - ptp_offset;
      }
      break;
    default:
      /* Ignore other cmsg options */
      break;
    }
  }
}
