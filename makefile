

SLAVE_SRC = sync_common.c sync_slave.c
MASTER_SRC = sync_common.c sync_master.c


slave : $(SLAVE_SRC)
	$(CC) $^ -o $@


master : $(MASTER_SRC)
	$(CC) $^ -o $@


all : master slave
