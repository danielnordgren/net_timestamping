
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <errno.h>
#include <time.h>
#include <getopt.h>

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <linux/net_tstamp.h>
#include <linux/sockios.h>

#ifndef SYNC_COMMON_H
#define SYNC_COMMON_H

#define hton16 htobe16
#define hton32 htobe32
#define hton64 htobe64


typedef struct {
  uint32_t type;
  uint32_t id;
  uint64_t sw_clock;
  uint64_t sw_timestamp;
  uint64_t hw_timestamp;
} msg_t;


extern const char* mcast_addr;
extern const int   sync_port;
extern const int   sfup_port;
extern const int   sync_period_ns;


extern uint64_t now();
extern uint64_t nsec(struct timespec* ts);
extern void wait_until(uint64_t ns);

extern void enable_hw_ts(int sock, const char* dev);

extern void enable_socket_timestamping(int sock);

extern void create_address(struct sockaddr_in* address, const char* ip, const int port);
extern int create_socket(void);

extern void parse_msg_timestamp(struct msghdr* msg,
				uint64_t* sw_ts,
				uint64_t* hw_ts);

#endif //SYNC_COMMON_H
